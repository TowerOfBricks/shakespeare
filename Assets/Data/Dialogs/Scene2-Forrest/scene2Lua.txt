
--[[
funktioner körs med
	namn (argument, argument)

vill du köra en funktion i en annan tråd så skriver du
	async (namn, argument, argument)

Till exempel
	async (moveTowards, "MyWaypoint")
Som kommer att köra moveTowards separat


"I en annan tråd" betyder att den nuvarande funktionen kommer att fortsätta köras medans den andra funktionen körs
vill du t.ex att två karaktärer ska röra sig samtidigt så kan du skriva

async (moveOrder, "Titania", "MyWaypoint")
async (moveOrder, "Oberon", "MySecondWaypoint")

Skriver du däremot
moveOrder ("Titania", "MyWaypoint")
moveOrder ("Oberon", "MySecondWaypoint")
Så kommer först Titania att gå, och sedan när hon är framme så kommer Oberon att börja gå.

==== Funktioner som du kan använda ====

log (argument)			- skriver ut ett objekt till loggen
say (text, seconds)		- skriver ut text som i en dialog, texten stannar uppe i [seconds] sekunder
wait (seconds)			- vänta i [seconds] sekunder (kan vara decimaltal)

waitFor ({lista av async operationer}) - vänta på att ett antal async operationer ska bli färdiga
	exempel:
		a1 = async(moveOrder, "Titania", "MyWaypoint")
		a2 = async(moveOrder, "Oberon", "MySecondWaypoint")
		waitFor ({a1,a2})
		say ("We have both reached our destinations",1)

]]

function followWaypoints(player, ...)
	log ("Stuff.")
	for i,v in ipairs(arg) do
		--log (v)
		walk (player, v)
	end
end

function help ()
	wait(30)
	sayAuto ("Hey, follow us!")
end

function start()
	
	async(say ,"Run! Run!!",4)
	wait(2)
	
	async (followWaypoints,"Hermia","W1","W2","W3","W4","W5","W6")
	wait(0.3)
	async (followWaypoints,"Lysander","W1","W2","W3","W4","W5","W6")
	wait(0.3)
	--async (followWaypoints,"Helena","W1","W2","W3","W4","W5","W6")
	wait(0.2)
	async (followWaypoints,"Demitrius","W1","W2","W3","W4","W5","W6")
	
	async(help)
	
	waitForProximity ("Helena","W6",20)
    
    sayAuto ("Yay, we have escaped to the forrest!! :D")
    
    loadScene("Scene3")
end

--Starta första funktionen, detta måste vara en async funktion
async(start)