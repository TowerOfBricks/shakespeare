using UnityEngine;
using System.Collections;

public class TimedObjectDestructor : MonoBehaviour {
	public float delay = 1;
	public bool detachChildren = false;
	
	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds (delay);
		if (detachChildren) transform.DetachChildren ();
		Destroy (gameObject);
	}
}
