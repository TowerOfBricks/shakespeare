using UnityEngine;
using System.Collections;
using System.Text;
using System;

[ExecuteInEditMode ()]
/** Profiles GC memory usage.
 * Attach to any GameObject to see statistics on GC memory and game fps.
 */
public class AllocMem : MonoBehaviour {

	public bool show = true;
	public bool showFPS = false;
	public bool showInEditor = false;
	public void Start () {
		useGUILayout = false;
		
		fpsDrops = new float[fpsDropCounterSize];
		
		for (int i=0;i<fpsDrops.Length;i++) {
			fpsDrops[i] = 1F / Time.deltaTime;
		}
	}
	
	StringBuilder text = new StringBuilder ();
	string cachedText;
	float lastUpdate = -999;
	
	// Use this for initialization
	public void OnGUI () {
		if (!show || (!Application.isPlaying && !showInEditor)) {
			return;
		}
		
		int collCount = System.GC.CollectionCount (0);
		
		if (lastCollectNum != collCount) {
			lastCollectNum = collCount;
			delta = Time.realtimeSinceStartup-lastCollect;
			lastCollect = Time.realtimeSinceStartup;
			lastDeltaTime = Time.deltaTime;
			collectAlloc = allocMem;
		}
		
		allocMem = (int)System.GC.GetTotalMemory (false);
		
		peakAlloc = allocMem > peakAlloc ? allocMem : peakAlloc;
		
		if (Time.realtimeSinceStartup - lastAllocSet > 0.3F) {
			int diff = allocMem - lastAllocMemory;
			lastAllocMemory = allocMem;
			lastAllocSet = Time.realtimeSinceStartup;
			delayedDeltaTime = Time.deltaTime;
			
			if (diff >= 0) {
				allocRate = diff;
			}
		}
		
		if (lastFrameCount != Time.frameCount) {
			fpsDrops[Time.frameCount % fpsDrops.Length] = 1F / Time.deltaTime;
		}
		
		if (cachedText == null || Time.realtimeSinceStartup	- lastUpdate > 0.2f) {
			lastUpdate = Time.realtimeSinceStartup;
			
			text.Length = 0;
			text.Append ("Currently allocated			");
			text.Append ((allocMem/1000000F).ToString ("0.0"));
			text.Append ("mb\n");
			
			text.Append ("Peak allocated				");
			text.Append ((peakAlloc/1000000F).ToString ("0.0"));
			text.Append ("mb (last	collect ");
			text.Append ((collectAlloc/1000000F).ToString ("0"));
			text.Append (" mb)\n");
			
			
			text.Append ("Allocation rate				");
			text.Append ((allocRate/1000000F).ToString ("0.0"));
			text.Append ("mb\n");
			
			text.Append ("Collection frequency		");
			text.Append (delta.ToString ("0.00"));
			text.Append ("s\n");
			
			text.Append ("Last collect delta			");
			text.Append (lastDeltaTime.ToString ("0.000"));
			text.Append ("s (");
			text.Append ((1F/lastDeltaTime).ToString ("0.0"));
			
			text.Append (" fps)");
			
			if (showFPS) {
				text.Append ("\nFPS").Append("								").Append((1F/delayedDeltaTime).ToString ("0.0")).Append(" fps");
				
				
				float minFps = Mathf.Infinity;
				
				for (int i=0;i<fpsDrops.Length;i++) {
					if (fpsDrops[i] < minFps) {
						minFps = fpsDrops[i];
					}
				}
				
				text.Append ("\nLowest fps  (last ").Append (fpsDrops.Length).Append(")		").Append(minFps.ToString ("0.0"));
			}
			
			cachedText = text.ToString();
		}
		
		GUI.Box (new Rect (5,5,310,80+(showFPS ? 32 : 0)),"");
		GUI.Label (new Rect (10,5,1000,200),cachedText);
		/*GUI.Label (new Rect (5,5,1000,200),
			"Currently allocated			"+(allocMem/1000000F).ToString ("0")+"mb\n"+
			"Peak allocated				"+(peakAlloc/1000000F).ToString ("0")+"mb "+
			("(last	collect"+(collectAlloc/1000000F).ToString ("0")+" mb)" : "")+"\n"+
			"Allocation rate				"+(allocRate/1000000F).ToString ("0.0")+"mb\n"+
			"Collection space			"+delta.ToString ("0.00")+"s\n"+
			"Last collect delta			"+lastDeltaTime.ToString ("0.000") + " ("+(1F/lastDeltaTime).ToString ("0.0")+")");*/
	}
	
	private float delayedDeltaTime = 1;
	private float lastCollect = 0;
	private float lastCollectNum = 0;
	private float delta = 0;
	private float lastDeltaTime = 0;
	private int allocRate = 0;
	private int lastAllocMemory = 0;
	private float lastAllocSet = -9999;
	private int allocMem = 0;
	private int collectAlloc = 0;
	private int peakAlloc = 0;
	
	private int lastFrameCount = -1;
	private int fpsDropCounterSize = 200;
	private float[] fpsDrops;
	
}
