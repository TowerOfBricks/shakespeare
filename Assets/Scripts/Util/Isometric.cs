using UnityEngine;
using System.Collections;

public class Isometric : MonoBehaviour {
	
	public bool rotate180 = false;
	
	// Use this for initialization
	[ContextMenu("Align")]
	void Start () {
		Camera cam = Camera.main;
		//Vector3 ang = transform.rotation.eulerAngles;
		//ang.z = cam.transform.rotation.z;
		//transform.rotation = Quaternion.Euler (new Vector3 (cam.transform.rotation.eulerAngles.x,0,0));
		transform.LookAt (transform.position - cam.transform.up, rotate180 ? Vector3.down : Vector3.up);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
