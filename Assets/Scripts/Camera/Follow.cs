using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {

	public Transform target;
	
	public Vector3 offset = Vector3.zero;
	public float dist = 5;
	
	// Update is called once per frame
	void LateUpdate () {
		if (target != null)
			transform.position = target.position - transform.forward*dist + offset;
	}
}
