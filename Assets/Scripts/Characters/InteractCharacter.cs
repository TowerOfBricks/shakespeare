using UnityEngine;
using System.Collections;

public class InteractCharacter : Character {
	
	public TextAsset scriptData;
	public TextAsset framework;
	
	public GameObject effect;
	public float effectScale = 1.0f;
	
	public InteractLuaScript script;
	
	
	// Use this for initialization
	void Start () {
		//script = new InteractScript(this);
		GameInterface gui = FindObjectOfType (typeof(GameInterface)) as GameInterface;
		script = new InteractLuaScript(framework.text,gui);
		script.Run (scriptData.text);
		StartCoroutine (script.RunScript());
	}
	
	public virtual void InteractClose (Player pl) {
	}
	
	public virtual void Interact (Player pl) {
		if (script.isRunning) return;
		
		if (effect != null) {
			GameObject go = GameObject.Instantiate (effect,transform.position,Quaternion.identity) as GameObject;
			go.transform.localScale *= effectScale;
		}
		//StartCoroutine (script.Run ());
		
		script.startInteract ();
		StartCoroutine (script.RunScript());
	}
}
