using UnityEngine;
using System.Collections;
using AluminumLua;

public class Player : Character {
	
	public float interactDistance = 4;
	
	public GameObject interactSymbol;
	
	// Use this for initialization
	void Start () {
		LuaContext lua = new LuaContext();
		lua.SetGlobal ("DoStuff",DoStuff);
		
		string script = "DoStuff(\"HI\")";
		
		LuaParser parser = new LuaParser(lua, script);
		parser.Parse();
		
	}
	
	public LuaObject DoStuff (LuaObject[] args) {
		Debug.Log ("Doing stuff");
		return new LuaObject();
	}
	
	public int move;
	
		
	public static bool interactSymbolShown = false;
	
	// Update is called once per frame
	void Update () {
		
		if (!interactSymbolShown) {
			InteractCharacter[] chs = FindObjectsOfType(typeof(InteractCharacter)) as InteractCharacter[];
			for (int i=0;i<chs.Length;i++) {
				InteractCharacter ch = chs[i];
				Vector3 pos = ch.transform.position;
				if ((pos - transform.position).sqrMagnitude < interactDistance*interactDistance) {
					GameObject.Instantiate (interactSymbol, pos, Quaternion.identity);
					interactSymbolShown = true;
					break;
				}
			}
		}
		
		if (Input.GetKeyDown (KeyCode.T)) {
			InteractCharacter[] chs = FindObjectsOfType(typeof(InteractCharacter)) as InteractCharacter[];
			for (int i=0;i<chs.Length;i++) {
				InteractCharacter ch = chs[i];
				Vector3 pos = ch.transform.position;
				if ((pos - transform.position).sqrMagnitude < interactDistance*interactDistance) {
					ch.Interact (this);
					break;
				}
			}
		}
		
		float dx = Input.GetAxis ("Horizontal");
		float dy = Input.GetAxis ("Vertical");
		
		Vector3 velocity = Vector3.zero;
		if (Mathf.Abs (dx) > 0.99f && (move == 0 || move == -1)) {
			move = 0;
			velocity.x += Mathf.Sign(dx);
		} else if (move == 0) move = -1;
		
		if (Mathf.Abs (dy) > 0.99f && (move == 1 || move == -1)) {
			move = 1;
			velocity.z += Mathf.Sign(dy);
		} else if (move == 1) move = -1;
		
		Move (velocity*Time.deltaTime*moveSpeed);
	}
	
}
