using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {
	
	CharacterController controller;
	public float moveSpeed;
	
	const float targetReachedDist = 8f;
	
	public UVAnimState normal, mvRight, mvLeft, mvForward, mvBack;
	
	protected AnimatedTexture at;
	
	// Use this for initialization
	void Awake () {
		at = GetComponentInChildren<AnimatedTexture>();
		controller = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	public void Move (Vector3 v) {
		controller.Move (v);
		Vector3 pos = transform.position;
		pos.y = 0;
		transform.position = pos;
		
		if (Mathf.Approximately (v.sqrMagnitude,0)) {
			Animate (at.state, true);
		} else if (Mathf.Abs (v.x) > Mathf.Abs (v.z)) {
			Animate (v.x > 0 ? mvRight : mvLeft, false);
		} else {
			Animate (v.z > 0 ? mvForward : mvBack, false);
		}
	}
	
	public void Animate (UVAnimState state, bool singleFrame) {
		at.state = state;
		at.singleFrame = singleFrame;
	}
	
	public bool MoveTowards (Vector3 p) {
		Vector3 dir = p - transform.position;
		dir.y = 0;
		
		if (dir.sqrMagnitude < targetReachedDist*targetReachedDist) {
			return true;
		}
		
		/*if (dir.x > targetReachedDist*0.5) {
			dir.z = 0;
		} else {
			dir.x = 0;
		}*/
		
		//dir = Vector3.ClampMagnitude (dir, moveSpeed);
		dir = dir.normalized * moveSpeed;
		
		Move (dir * Time.deltaTime);
		return false;
	}
}
