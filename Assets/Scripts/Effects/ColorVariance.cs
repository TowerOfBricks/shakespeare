using UnityEngine;
using System.Collections;

public class ColorVariance : MonoBehaviour {
	
	public Color a = Color.green, b = Color.yellow;
	public bool totallyRandom = false;
	public bool randomRotY = false;
	
	// Use this for initialization
	void Start () {
		Color col = totallyRandom ? new Color(Random.value,Random.value,Random.value) : Color.Lerp(a,b,Random.value);
		Renderer[] rends = GetComponentsInChildren<Renderer>();
		for (int i=0;i<rends.Length;i++) {
			for (int j=0;j<rends[i].materials.Length;j++)
				rends[i].materials[j].color = col;
	
		}
		
		if (randomRotY) {
			Vector3 rot = transform.eulerAngles;
			rot.y = Random.value*360;
			transform.eulerAngles = rot;
		}
	}
}
