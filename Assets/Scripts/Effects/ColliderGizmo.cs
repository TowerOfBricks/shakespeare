using UnityEngine;
using System.Collections;

public class ColliderGizmo : MonoBehaviour {

	// Use this for initialization
	void OnDrawGizmos () {
		Gizmos.color = new Color(0,1,0,0.1f);
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawCube (Vector3.zero,Vector3.one);
		Gizmos.DrawWireCube (Vector3.zero,Vector3.one);
	}
}
