using UnityEngine;
using System.Collections;

public class WaypointGizmo : MonoBehaviour {

	// Use this for initialization
	void OnDrawGizmos () {
		Gizmos.color = new Color (0.2f,0.2f,1,0.5f);
		
		for (int i=0;i<transform.childCount;i++) {
			Gizmos.DrawCube (transform.GetChild(i).position,Vector3.one*2);
			Gizmos.DrawRay (transform.GetChild(i).position,Vector3.up*2);
		}
	}
}
