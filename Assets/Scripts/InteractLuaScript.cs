using UnityEngine;
using System.Collections;
//using AluminumLua;
using LuaInterface;
using Lua511;

public class InteractLuaScript {
	
	//LuaContext lua;
	//LuaParser parser;
	
	Lua lua;
	
	LuaInterface.LuaFunction tick;
	GameInterface gui;
	
	public bool isRunning = false;
	
	public InteractLuaScript (string framework, GameInterface gui) {
		this.gui = gui;
		/*lua = new LuaContext();
		lua.SetGlobal("log",LuaLog);
		
		parser = new LuaParser(lua,text.text);*/
		
		//parser.Parse (true);
		lua = new Lua();
		
		lua.RegisterFunction ("tickCoroutine",this,typeof(InteractLuaScript).GetMethod("TickCoroutine"));
		
		//lua.RegisterFunction ("say",this,typeof(InteractLuaScript).GetMethod("say"));
		//lua.RegisterFunction ("_wait",this,typeof(InteractLuaScript).GetMethod("wait"));
		
		AddFunc ("log");
		AddFunc ("logError");
		AddFunc ("loadScene");
		AddFunc ("getQuestionAnswer");
		AddFunc ("setActiveGO");
		AddCoroutine ("wait");
		AddCoroutine ("say");
		AddCoroutine ("sayAuto");
		AddCoroutine ("walk");
		AddCoroutine ("ask1");
		AddCoroutine ("ask2");
		AddCoroutine ("ask3");
		AddCoroutine ("ask4");
		AddCoroutine ("waitForProximity");
		lua.DoString (framework);
		tick = lua.GetFunction ("tick");
		
		
	}
	
	public void startInteract () {
		Debug.Log ("Starting Interact");
		lua.GetFunction("startInteract").Call (new object[0]);
	}
	
	public void AddFunc (string name) {
		System.Reflection.MethodInfo info = typeof(InteractLuaScript).GetMethod(name);
		lua.RegisterFunction (name,this,info);
	}
	
	public void AddFunc (string name, string luaName) {
		System.Reflection.MethodInfo info = typeof(InteractLuaScript).GetMethod(name);
		lua.RegisterFunction (luaName,this,info);
	}
	
	public void AddCoroutine (string name) {
		System.Reflection.MethodInfo info = typeof(InteractLuaScript).GetMethod(name);
		
		lua.RegisterFunction ("_"+name,this,info);
		lua.DoString (
			"function " + name + " (...) \n runUnity (_"+name+",unpack(arg)) \n end"
			);
	}
	
	public void AddCoroutine (string name, string luaName) {
		System.Reflection.MethodInfo info = typeof(InteractLuaScript).GetMethod(name);
		
		lua.RegisterFunction ("_"+name,this,info);
		lua.DoString (
			"function " + luaName + " (...) \n runUnity (_"+name+",unpack(arg)) \n end"
			);
	}
	
	public void Run (string text) {
		lua.DoString(text);
	}
	
	public IEnumerator RunScript () {
		isRunning = true;
		while (true) {
			//parser.Parse (true);
			object[] objs = tick.Call (new object[0]);
			//Debug.Log ("SIZE: " + objs.Length);
			//Debug.LogError (objs[0]);
			if ((bool)objs[0] == true) {
				isRunning = false;
				yield break;
			}
			yield return 0;
		}
	}
	
	/*LuaObject LuaLog (LuaObject[] args) {
		Debug.Log (args[0].AsString());
		return new LuaObject();
	}*/
	
	public bool TickCoroutine (IEnumerator co) {
		return co.MoveNext();
	}
	
	
#region LuaFunctions
	
	public void log (string arg) {
		Debug.Log (arg);
	}
	
	public void logError (string arg) {
		Debug.LogError (arg);
	}
	
	public void loadScene (string scene) {
		Application.LoadLevel (scene);
	}
	
	public void setActiveGO (string obj, bool state) {
		GameObject ob = GameObject.Find (obj);
		if (ob == null) {
			logError ("Could not find any gameObject with the name " + obj);
			return;
		}
		ob.SetActiveRecursively (state);
	}
	
	public IEnumerator ask1 (string text, string alt1) {
		IEnumerator en = ask(text, new string[] {alt1});
		while (en.MoveNext()) yield return 0;
	}
	
	public IEnumerator ask2 (string text, string alt1, string alt2) {
		IEnumerator en = ask(text, new string[] {alt1,alt2});
		while (en.MoveNext()) yield return 0;
	}
	
	public IEnumerator ask3 (string text, string alt1, string alt2, string alt3) {
		IEnumerator en = ask(text, new string[] {alt1,alt2,alt3});
		while (en.MoveNext()) yield return 0;
	}
	
	public IEnumerator ask4 (string text, string alt1, string alt2, string alt3, string alt4) {
		IEnumerator en = ask(text, new string[] {alt1,alt2,alt3,alt4});
		while (en.MoveNext()) yield return 0;
	}
	
	public IEnumerator ask (string text, string[] alts) {
		gui.ShowQuestion (text,alts);
		while (!gui.HasAnsweredQuestion()) yield return 0;
	}
	
	public int getQuestionAnswer () {
		return gui.GetQuestionAnswer ();
	}
	
	public IEnumerator say (string text, float time) {
		gui.showDialogText(text);
		time += Time.time;
		while (Time.time <= time && !Input.GetKeyDown(KeyCode.Escape)) yield return 0;
		gui.hideText (text);
	}
	
	public IEnumerator sayAuto (string text) {
		float time = 1.5f + 0.05f*text.Length;
		gui.showDialogText(text);
		time += Time.time;
		while (Time.time <= time && !Input.GetKeyDown(KeyCode.Escape)) yield return 0;
		gui.hideText (text);
	}
	
	public IEnumerator wait (float time) {
		time += Time.time;
		while (Time.time < time && !Input.GetKeyDown(KeyCode.Escape)) yield return 0;
	}
	
	public IEnumerator waitForProximity (string objName1, string objName2, float dist) {
		GameObject ob = GameObject.Find(objName1);
		if (ob == null) { logError ("Cannot find object named " + objName1); yield break; }
		
		GameObject wp = GameObject.Find (objName2);
		if (wp == null) { logError ("Cannot find object named " + objName2); yield break; }
		while (Vector3.Distance (ob.transform.position, wp.transform.position) > dist) yield return 0;
	}
	
	public IEnumerator walk (string player, string waypoint) {
		GameObject ob = GameObject.Find(player);
		if (ob == null) { logError ("Cannot find object named " + player); yield break; }
		Character ch = ob.GetComponent<Character>();
		if (ch == null) { logError (player + " is not a character"); yield break; }
		
		GameObject wp = GameObject.Find (waypoint);
		if (wp == null) { logError ("Cannot find object named " + waypoint); yield break; }
		
		while(!ch.MoveTowards (wp.transform.position) && !Input.GetKeyDown(KeyCode.Escape)) yield return 0;
		ch.Move (Vector3.zero);
	}
#endregion
	
}

