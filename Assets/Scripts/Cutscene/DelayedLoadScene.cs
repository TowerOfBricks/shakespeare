using UnityEngine;
using System.Collections;

public class DelayedLoadScene : MonoBehaviour {

	public float delay = 5;
	public int scene = 0;
	
	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds (delay);
		Application.LoadLevel (scene);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			Application.LoadLevel (scene);
		}
	}
}
