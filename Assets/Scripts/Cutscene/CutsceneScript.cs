using UnityEngine;
using System.Collections;

public class CutsceneScript : MonoBehaviour {
	
	public TextAsset framework;
	public TextAsset scriptData;
	public InteractLuaScript script;
	
	public bool runOnStart = false;
	public float delay = 0;
	
	public GUISkin gskin;
	
	// Use this for initialization
	IEnumerator Start () {
		//script = new InteractScript(this);
		//script.gskin = gskin;
		
		if (scriptData != null) {
			GameInterface gui = FindObjectOfType (typeof(GameInterface)) as GameInterface;
			Debug.Log ("Loading Script Data");
			//script.Load (scriptData);
			script = new InteractLuaScript(framework.text,gui);
			script.Run (scriptData.text);
			StartCoroutine (script.RunScript());
		}
		yield return new WaitForSeconds (delay);
		//if (runOnStart) StartCoroutine (script.Run ());
	}
	
	public void OnGUI () {
		//script.OnGUI ();
	}
}
