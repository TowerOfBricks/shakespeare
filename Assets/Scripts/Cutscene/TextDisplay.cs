using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TextMesh))]
public class TextDisplay : MonoBehaviour {
	
	public float[] times;
	public string[] text;
	
	public Animation anim;
	public float zoom = 1;
	
	// Use this for initialization
	IEnumerator Start () {
		Vector3 startScale = transform.localScale;
		
		TextMesh tx = GetComponent<TextMesh>();
		
		if (text.Length != times.Length) {
			Debug.LogError ("Text count not equal to times count");
			yield break;
		}
		
		for (int i=0;i<text.Length;i++) {
			tx.text = text[i];
			foreach (AnimationState s in anim) s.speed = 1.0f/times[i];
			
			
			float stime = Time.time;
			while (Time.time - stime < times[i]) {
				transform.localScale = startScale * (1+(Time.time-stime)*zoom);
				yield return 0;
			}
		}
		foreach (AnimationState s in anim) s.wrapMode = WrapMode.Clamp;
		//foreach (AnimationState s in anim) s.speed = -1.0f/2;
	}
}
