using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using Pathfinding.Serialization.JsonFx;

[Serializable]
public class InteractScript : System.Object {
	
	public List<InteractEvent> events = new List<InteractEvent>();
	
	private int currentIndex = -1;
	
	public InteractCharacter caller;
	public MonoBehaviour monoCaller;
	public GUISkin gskin;
	
	public List<InteractEvent> async = new List<InteractEvent>();
	
	public InteractScript (InteractCharacter caller) : this((MonoBehaviour)caller){
		this.caller = caller;
	}
	
	public InteractScript (MonoBehaviour monoCaller) {
		this.monoCaller = monoCaller;
		
		monoCaller.StartCoroutine (RunAsync ());
	}
	
	public void Load (TextAsset text) {
		events.Clear ();
		StringReader str = new StringReader(text.text);
		JsonReader reader = new JsonReader(str);
		while (true) {
			string typeName = reader.Deserialize (typeof(string)) as string;
			
			Debug.LogWarning ("Read " + (typeName == null ? "NULL" : typeName));
			if (typeName == null) break;
			if (typeName == "" || typeName.Trim().StartsWith("#")) continue;
			
			Type type = Type.GetType ("Interact"+typeName);
			if (type == null) {
				Debug.LogWarning ("Could not find event of type Interact"+typeName);
				break;
			}
			
			
			InteractEvent ev = reader.Deserialize (type) as InteractEvent;
			events.Add (ev);
		}
		str.Close ();
	}
	
	public IEnumerator RunAsync () {
		while (true) {
			//Update async stuff
			for (int i=async.Count-1;i>=0;i--) {
				InteractEvent acurrent = async[i];
				
				if (acurrent.Update(this) == InteractStatus.Next) {
					async.RemoveAt(i);
				}
			}
			yield return 0;
		}
	}
	
	public IEnumerator Run () {
		currentIndex = 0;
		while (currentIndex < events.Count) {
			
			InteractEvent current = events[currentIndex];
			current.Start(this);
			while (current != null) {
				if (current.async) {
					async.Add (current);
					break;
				} else {
					InteractStatus rt = current.Update (this);
					if (rt == InteractStatus.Abort) yield break;
					else if (rt == InteractStatus.Next) break;
				}
				yield return 0;
			}
			currentIndex++;
			yield return 0;
		}
	}
	
	public IEnumerator Run (InteractEvent current) {
		while (true) {
			InteractStatus rt = current.Update (this);
			if (rt == InteractStatus.Abort) yield break;
			else if (rt == InteractStatus.Next) break;
			yield return 0;
		}
	}
	
	public void OnGUI () {
		if (currentIndex >= 0 && currentIndex < events.Count) {
			if (gskin != null) GUI.skin = gskin;
			InteractStatus rt = events[currentIndex].OnGUI (this);
			
			if (rt == InteractStatus.Abort) currentIndex = events.Count;
			else if (rt == InteractStatus.Next) {
				currentIndex++;
			}
		}
	}
}

[Serializable]
public abstract class InteractEvent : System.Object {
	public bool async = false;
	public abstract void Start(InteractScript script);
	public abstract InteractStatus Update(InteractScript script);
	public virtual InteractStatus OnGUI (InteractScript script) { return InteractStatus.Continue; }
	
}

public class WaitForAsync : InteractEvent {
	public override void Start (InteractScript script) {}
	public override InteractStatus Update (InteractScript script) {
		if (script.async.Count == 0) return InteractStatus.Next;
		return InteractStatus.Continue;
	}
}

public class InteractSay : InteractEvent {
	public string text = "I say nay!";
	
	const float textTokenTime = 0.05f;
	const float textMinTime = 1f;
	
	private float sayTime = -9999;
	
	public override void Start (InteractScript script) {
		sayTime = Time.time;
		Debug.Log (text);
	}
	
	public override InteractStatus Update (InteractScript script) {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			return InteractStatus.Abort;
		}
		
		if (Time.time > sayTime + textMinTime + textTokenTime*text.Length || Input.GetKeyDown (KeyCode.Space)) {
			return InteractStatus.Next;
		}
		return InteractStatus.Continue;
	}
	
	public override InteractStatus OnGUI (InteractScript script) {
		GUI.Label (new Rect (5,Screen.height-50,Screen.width-10,45),text);
		return InteractStatus.Continue;
	}
}

public class InteractMoveOrder : InteractEvent {
	public string unitName = null;
	GameObject targetObj = null;
	public string[] targetObjs = null;
	public Vector3 targetPos;
	InteractCharacter unit = null;
	int waypointIndex = 0;
	
	public override void Start (InteractScript script) {
		if (unitName != null) {
			GameObject go = GameObject.Find (unitName);
			if (go == null) throw new NullReferenceException ("Could not find GameObject with name "+targetObj+", write better!");
			unit = go.GetComponent<InteractCharacter>();
			if (unit == null) throw new NullReferenceException ("That GameObject does not have an InteractCharacter script attached ("+targetObj+"), write better!");
		}
		if (unit == null) unit = script.caller;
		
		if (targetObj == null) {
			targetObj = findTargetObj (targetObjs[waypointIndex]);
			targetPos = targetObj.transform.position;
		}
	}
	
	public GameObject findTargetObj (string name) {
		GameObject go = GameObject.Find (name);
		if (go == null) throw new NullReferenceException ("Could not find GameObject with name "+name+", write better!");
		return go;
	}
	
	public override InteractStatus Update (InteractScript script) {
		
		if (unit == null) {
			throw new NullReferenceException ("No unit to order was supplied");
		}
		
		if (!unit.MoveTowards (targetPos)) {
			return InteractStatus.Continue;
			
		} else {
			waypointIndex++;
			
			if (waypointIndex >= targetObjs.Length) {
				Debug.Log ("End Move");
				unit.Move (Vector3.zero);
				return InteractStatus.Next;
			}
			Debug.Log ("Next Waypoint... " + targetObjs[waypointIndex]);
			targetObj = findTargetObj (targetObjs[waypointIndex]);
			targetPos = targetObj.transform.position;
		}
		
		return InteractStatus.Continue;
	}
}

public class InteractLoadScene : InteractEvent {
	string scene = null;
	int sceneID = 0;
	
	public override void Start (InteractScript script) {
		if (String.IsNullOrEmpty (scene)) Application.LoadLevel (sceneID);
		else Application.LoadLevel (scene);
	}
	public override InteractStatus Update (InteractScript script) {
		return InteractStatus.Abort;
	}
}

public class InteractAnimation : InteractEvent {
	string scene = null;
	int sceneID = 0;
	
	public override void Start (InteractScript script) {
		if (String.IsNullOrEmpty (scene)) Application.LoadLevel (sceneID);
		else Application.LoadLevel (scene);
	}
	public override InteractStatus Update (InteractScript script) {
		return InteractStatus.Abort;
	}
}

public class InteractAsk {
	public string text = "Shall I compare thee to a Summers day?";
	public string[] alt;
	
}

public enum InteractStatus {
	Abort,
	Next,
	Continue
}

public enum InteractMode {
	Say,
	Ask,
	Walk
}