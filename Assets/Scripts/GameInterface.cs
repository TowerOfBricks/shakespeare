using UnityEngine;
using System.Collections;

public class GameInterface : MonoBehaviour {

	public GUISkin gskin;
	
	private string dialogText = null;
	
	private string questionText = null;
	private string[] alts = null;
	private int questionAnswer = -1;
	private bool hasAnsweredQuestion = false;
	
	public void showDialogText (string text) {
		dialogText = text;
	}
	
	public void ShowQuestion (string text, string[] alternatives) {
		dialogText = null;
		hasAnsweredQuestion = false;
		alts = alternatives;
		questionText = text;
	}
	
	public int GetQuestionAnswer () {
		return questionAnswer;
	}
	
	public bool HasAnsweredQuestion () {
		return hasAnsweredQuestion;
	}
	
	public void hideText () {
		dialogText = null;
	}
	
	/** Hide text only if the current text is the specified text */
	public void hideText (string text) {
		if (dialogText == text) hideText ();
	}
	
	public void OnGUI () {
		//Don't like that operator, but it's handy
		GUI.skin = gskin ?? GUI.skin;
		
		if (dialogText != null) {
			GUI.Box (new Rect (5,Screen.height-50, Screen.width-10,45),"");
			GUI.Label (new Rect (5,Screen.height-50, Screen.width-10,45),dialogText);
		}
		
		if (questionText != null && !hasAnsweredQuestion) {
			GUI.Box (new Rect (5,Screen.height-50, Screen.width-10,45),"");
			GUI.Label (new Rect (5,Screen.height-50, Screen.width-10,45),questionText);
			
			for (int i=0;i<alts.Length;i++) {
				int sel = GUI.SelectionGrid (new Rect (100,Screen.height/2 + 50, Screen.width-200, 100), -1, alts,alts.Length);
				if (sel >= 0) {
					questionAnswer = sel;
					hasAnsweredQuestion = true;
				}
			}
		}
		
	}
}
